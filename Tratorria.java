
public class Tratorria extends Restaurant {
    String name = "Tratoria Del Italia";
    Employee[] employeesList = {new Ceo(), new Manager(), new Cooker()};


    void traditionalFood(){
        System.out.println("There is no traditional Moldavian food because this is Italian restaurant.");
        super.showUnderline();
    }

    void getEmployers(){
        for(Employee employees : employeesList) {
            employees.jobDescription();
        }
    }

    String getName (){
        // we return the name of current object
        return this.name;
    }
}
