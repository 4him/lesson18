import java.util.ArrayList;

abstract public class Restaurant {
    String name;
    abstract void traditionalFood();
    abstract void getEmployers();
    abstract String getName();

    // Using polymorphism do print name and underline depending of current object (2 different methods).
    void showUnderline(){
        int nameLength = this.getName().length();
        for(int i=0; i<=nameLength; i++){
            System.out.print("-");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        Restaurant[] allRest = {new LaPlacinte(), new Tratorria()};

        for(Restaurant singleRest : allRest){
            System.out.println(singleRest.getName());
            singleRest.showUnderline();
            singleRest.traditionalFood();
            singleRest.getEmployers();
            System.out.println();
        }
    }
}
