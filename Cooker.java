class Cooker extends Employee {
    String job = "Cooker. I cook food";

    // we use extension for Cooker class to make one of LaPlacinte cookers to be famous cooker
    Cooker(String new_job){
        super.job = "Cooker and " + new_job;
    }

    Cooker(){
        super.job = this.job;
    }
}
