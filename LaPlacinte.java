import java.util.ArrayList;
import java.util.List;

public class LaPlacinte extends Restaurant {
    String name = "La Placinte";
    private List<String> menu = new ArrayList<>();
    Employee[] employeesList = {new Ceo(), new Manager(), new Cooker("Pizza maker for 3 * of Micheline"), new Cooker(), new Manager()};


    void traditionalFood(){
        menu.add("Mamaliga");
        menu.add("Placinte");
        System.out.println("You can have traditional food in our restaurant "+menu.toString());
        super.showUnderline();
    }

    void getEmployers(){
        for(Employee employees : employeesList) {
            employees.jobDescription();
        }
    }

    String getName (){
        // we change name of super object
       return super.name = this.name;
    }
}
